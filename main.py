import re, os
import pymorphy2

def make_dictionary(file):
    stress_dictionary = {}
    for line in file:
        if '...' in line:
            prefix_regexp = re.search('^([а-яё]+)', line)
            prefix = prefix_regexp.group(1)
        else:
            prefix = ''
        endings = re.findall('\'([а-яё]*)\'', line)
        if prefix not in stress_dictionary:
            stress_dictionary[prefix] = {'nom_sg': [endings[0]], 'gen_sg': [endings[1]], 'dat_sg': [endings[2]], \
                                         'acc_sg': [endings[3]], 'ins_sg': [endings[4]], 'pre_sg': [endings[5]], \
                                         'nom_pl': [endings[6]], 'gen_pl': [endings[7]], 'dat_pl': [endings[8]], \
                                         'ins_pl': [endings[9]], 'pre_pl': [endings[10]]}
        else:
            stress_dictionary[prefix]['nom_sg'] += [endings[0]]
            stress_dictionary[prefix]['gen_sg'] += [endings[1]]
            stress_dictionary[prefix]['dat_sg'] += [endings[2]]
            stress_dictionary[prefix]['acc_sg'] += [endings[3]]
            stress_dictionary[prefix]['ins_sg'] += [endings[4]]
            stress_dictionary[prefix]['pre_sg'] += [endings[5]]
            stress_dictionary[prefix]['nom_pl'] += [endings[6]]
            stress_dictionary[prefix]['gen_pl'] += [endings[7]]
            stress_dictionary[prefix]['dat_pl'] += [endings[8]]
            stress_dictionary[prefix]['ins_pl'] += [endings[9]]
            stress_dictionary[prefix]['pre_pl'] += [endings[10]]            
        
    return(stress_dictionary)

def sort_by_length(string):
    return len(string)

morph = pymorphy2.MorphAnalyzer()

prefixes = open('data/prefixes_list.txt', 'r', encoding='utf-8').read()
prefixes_list = re.findall('[а-яё]+', prefixes)

on_prefix = open('data/prefix.txt', 'r', encoding='utf-8')
on_first_vowel = open('data/first_vowel.txt', 'r', encoding='utf-8')
on_root = open('data/root.txt', 'r', encoding='utf-8')
on_suffix = open('data/suffix.txt', 'r', encoding='utf-8')
on_suffix_1 = open('data/suffix_1.txt', 'r', encoding='utf-8')
on_suffix_2 = open('data/suffix_2.txt', 'r', encoding='utf-8')
on_presuffix = open('data/presuffix.txt', 'r', encoding='utf-8')
type_B = open('data/type_B.txt', 'r', encoding='utf-8')
type_D = open('data/type_D.txt', 'r', encoding='utf-8')

accentuation = {'prefix': make_dictionary(on_prefix), 'first_vowel': make_dictionary(on_first_vowel), \
                'root': make_dictionary(on_root), 'suffix': make_dictionary(on_suffix), \
                'suffix_1': make_dictionary(on_suffix_1), 'suffix_2': make_dictionary(on_suffix_2), \
                'presuffix': make_dictionary(on_presuffix), 'type_B': make_dictionary(type_B), \
                'type_D': make_dictionary(type_D)}



path = os.path.join(os.getcwd(), 'input')
files = os.listdir(path)
short_files_rule = re.compile('.txt')
for file in files:

    output_file = open(os.path.join(os.getcwd(), 'output', file), 'w', encoding='utf-8')
    
    if short_files_rule.search(file) != None:
        current_file = open(os.path.join(path, file), 'r', encoding='utf-8').read()
        processing_text = re.findall('([^а-яёА-ЯЁ]+)?([а-яёА-ЯЁ]+)', current_file)
        splitted_text = []
        for pair in processing_text:
            for element in pair:
                splitted_text.append(element)

        for word in splitted_text:
            new_word = ''

            if re.search('^[а-яёА-ЯЁ]+', word) != None:
                if 'NOUN' in morph.parse(word)[0].tag:
                    gram = ''
                    if 'nomn' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'nom_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'nom_pl'
                    if 'gent' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'gen_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'gen_pl'
                    if 'datv' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'dat_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'dat_pl'
                    if 'accs' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'acc_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'acc_pl'
                    if 'ablt' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'ins_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'ins_pl'
                    if 'loct' in morph.parse(word)[0].tag:
                        if 'sing' in morph.parse(word)[0].tag: gram = 'pre_sg'
                        elif 'plur' in morph.parse(word)[0].tag: gram = 'pre_pl'

                    stress_check = False
                    behavior_possible = []

                    word_lower = word.lower()
                    prefix_found = []
                    for prefix in prefixes_list:
                        if re.search('^'+prefix, word_lower) != None:
                            prefix_found.append(prefix)
                    if prefix_found != []:
                        prefix_found.sort(key=sort_by_length, reverse=True)

                        for behavior in accentuation:
                            if prefix_found[0] in accentuation[behavior]:
                                if gram != '':
                                    suffix_list = accentuation[behavior][prefix_found[0]][gram]
                                    for suffix in suffix_list:
                                        if suffix != '':
                                            if re.search(suffix+'$', word_lower) != None:
                                                behavior_possible.append([behavior, suffix])
                                                stress_check = True
                                
                    if stress_check == False:
                        for behavior in accentuation:
                            if '' in accentuation[behavior]:
                                if gram != '':
                                    suffix_list = accentuation[behavior][''][gram]
                                    for suffix in suffix_list:
                                        if suffix != '':
                                            if re.search(suffix+'$', word_lower) != None:
                                                behavior_possible.append([behavior, suffix])
                                                stress_check = True
                                
                    if len(behavior_possible) > 1:
                        suffix_found = []
                        for pair in behavior_possible:
                            suffix_found.append(pair[1])
                        suffix_found.sort(key=sort_by_length, reverse=True)
                        for pair in behavior_possible:
                            if suffix_found[0] == pair[1]:
                                behavior_possible = pair
                                break
                    elif len(behavior_possible) == 1:
                        behavior_possible = behavior_possible[0]

                    print(word, behavior_possible, gram)

                    if behavior_possible != []:
                        if behavior_possible[0] == 'prefix' or behavior_possible[0] == 'first_vowel':
                            part_of_word = re.findall('^[^уеыаоэяиюё]*[уеыаоэяиюё]', word_lower)
                        elif behavior_possible[0] == 'suffix' or behavior_possible[0] == 'suffix_1':
                            part_of_suffix = re.findall('^[^уеыаоэяиюё]*[уеыаоэяиюё]', behavior_possible[1])[0]
                            part_of_word = re.findall('^.+'+part_of_suffix, word_lower)
                        elif behavior_possible[0] == 'suffix_2':
                            part_of_suffix = re.findall('^([^уеыаоэяиюё]*[уеыаоэяиюё]){2}', behavior_possible[1])[0]
                            part_of_word = re.findall('^.+'+part_of_suffix, word_lower)
                        elif behavior_possible[0] == 'presuffix':
                            part_of_word = re.findall('^(.+?)[^уеыаоэяиюё]*?'+behavior_possible[1]+'$', word_lower)
                        elif behavior_possible[0] == 'type_B':
                            part_of_word = re.findall('^(.+[уеыаоэяиюё])[^уеыаоэяиюё]*$', word_lower)
                        new_word = word[:len(part_of_word[0])]+'\''+word[len(part_of_word[0]):]
                        print(new_word)

            if new_word == '':
                new_word = word

            
            
            output_file.write(new_word)
    output_file.close()
                
